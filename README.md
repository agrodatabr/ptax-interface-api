<p align="center">
    <img src="logo-mini.png" alt="logo"/>
</p>
<p align="center">
    &nbsp;<img src="https://img.shields.io/packagist/v/agrodata/ptax.svg" />
     <img src="https://img.shields.io/packagist/dt/agrodata/ptax.svg" />
</p>
## Ptax Service
Pacote responsável por todo gerenciamento dos valores de PTAX utilizados pelos sistemas.

Para saber mais, acesse a documentação do microserviço:
* https://bitbucket.org/agrodatabr/ptax-service-api

Essa é a interface responsável pela comunicação entre os projetos e nosso serviço PTAX.


### Instalação
#### Laravel / Lumen
Instale o pacote através do composer com o seguinte comando:

```
composer require agrodata/ptax
```

#### *Apenas para "Lumen framework"
Registre o arquivo de provider em ```bootstrap/app.php```
adicionando a seguinte linha no final do arquivo.

```php
$app->register(\Agrodata\PtaxServiceProvider::class);
```

### Configuração
Para configurar a biblioteca utilize o seguinte comando, que irá registrar o provider e criar o arquivo de configuração (opcional) dentro do projeto.
O arquivo de configuração apenas a URL da api de microserviço de ptax (```AGRODATA_PTAX_URL```)

```
php artisan agrodata-ptax:install
```

```php
<?php # config/agrodata-ptax.php

return [
    'url' => env('AGRODATA_PTAX_URL', 'https://ptax.agrodata.agr.br')
];
```

### Utilização

Após configurado, a classe \Agrodata\Ptax\PtaxService::class,
poderá ser instanciada em qualquer lugar do projeto, podendo assim, chamar os métodos 
de manipulação do microserviço de ptax;

#### ptax(string $date, [bool $strict = false]): string
Receberá uma data formatada em "Y-m-d" e um parâmetro opcional chamado strict, que caso true pega a data estritamente do dia especificado, ou em caso false, pega a data de d-1.

Retornará um array contendo em exemplo...  
```json
{
    "date": "2023-07-24", //data
    "buy": "4.7451", //valor de compra
    "sell": "4.7457", //valor de venda
    "average": "4.7454" //média entre a compra e venda
};
```
```php
<?php

use Agrodata\Ptax\PtaxService;

$ptaxValues = (new PtaxService)->ptax('2023-07-25');
```

### Fluxo de utilização de ptax completa
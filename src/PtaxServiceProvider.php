<?php

namespace Agrodata\Ptax;

use Illuminate\Support\ServiceProvider;

class PtaxServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPublishing();
        $this->mergeConfigFrom(__DIR__ . '/../config/agrodata-ptax.php', 'agrodata-ptax');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \Agrodata\Ptax\Console\InstallPtaxConsole::class
        ]);
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    private function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/agrodata-ptax.php' => base_path('config/agrodata-ptax.php'),
            ], 'config');
        }
    }
}

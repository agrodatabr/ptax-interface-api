<?php

namespace Agrodata\Ptax\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallPtaxConsole extends Command
{
    protected $signature = 'agrodata-ptax:install';
    protected $description = 'Install the Agrodata Ptax Service';

    public function handle()
    {
        $this->info('Installing Ptax Service...');
        $this->info('Publishing configuration...');

        if (! $this->configExists('agrodata-ptax.php')) {
            $this->publishConfiguration();
            $this->info('Published configuration');
        } else {
            if ($this->shouldOverwriteConfig()) {
                $this->info('Overwriting configuration file...');
                $this->publishConfiguration($force = true);
            } else {
                $this->info('Existing configuration was not overwritten');
            }
        }

        $this->info('Installed Ptax Service');
    }

    private function configExists($fileName)
    {
        return File::exists(config_path($fileName));
    }

    private function shouldOverwriteConfig()
    {
        return $this->confirm(
            'Config file already exists. Do you want to overwrite it?',
            false
        );
    }

    private function publishConfiguration($forcePublish = false)
    {
        $params = [
            '--provider' => "Agrodata\Ptax\PtaxServiceProvider",
            '--tag' => "config"
        ];

        if ($forcePublish === true) {
            $params['--force'] = true;
        }

        $this->call('vendor:publish', $params);
    }
}
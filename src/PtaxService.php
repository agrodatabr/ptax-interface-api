<?php

namespace Agrodata\Ptax;

use GuzzleHttp\Client;
use Carbon\Carbon;

class PtaxService
{
    private string $url;

    public function __construct(string $apiUrl = null)
    {
        $this->url = $apiUrl ?? config('agrodata-ptax.url');
    }

    private function callApi(string $method, string $path, array $params, string $paramType)
    {
        return (new Client())
            ->$method("{$this->url}{$path}", [ $paramType => [...$params] ])
            ->getBody()
            ->getContents();
    }

    private function get(string $path, array $params = [])
    {
        return $this->callApi('get', $path, $params, 'query');
    }

    public function ptax(Carbon $date, bool $strict = false): array
    {
        return json_decode(
            $this->get('',  [ 'date' => $date->format('Y-m-d'), 'dminusone' => (int)!$strict]), true);
    }

    public function buy(Carbon $date, bool $strict = false): float
    {
        return (float)($this->ptax($date, $strict)['buy'] ?? 0);
    }

    public function sell(Carbon $date, bool $strict = false): float
    {
        return (float)($this->ptax($date, $strict)['sell']) ?? 0;
    }

    public function average(Carbon $date, bool $strict = false): float
    {
        return (float)($this->ptax($date, $strict)['average']) ?? 0;
    }
}

<?php

return [
    'url' => env('AGRODATA_PTAX_URL', 'https://ptax.agrodata.agr.br')
];
